const db = require("../config/db.js");
const User = db.user;
const Role = db.role;
const Book = db.book;
const asyncMiddleware = require("express-async-handler");

exports.users = asyncMiddleware(async (req, res) => {
    const user = await User.findAll({
        attributes: ["name", "username", "email"],
        include: [
            {
                model: Role,
                attributes: ["id", "name"],
                through: {
                    attributes: ["userId", "roleId", "status"]
                }
            }
        ]
    });
    res.status(200).json({
        description: "All User",
        user: user
    });
});

exports.userContent = asyncMiddleware(async (req, res) => {
    const user = await User.findOne({
        where: { id: req.userId },
        attributes: ["name", "username", "email"],
        include: [
            {
                model: Role,
                attributes: ["id", "name"],
                through: {
                    attributes: ["userId", "roleId"]
                }
            }
        ]
    }

    );
    res.status(200).json({
        description: "User Content Page",
        user: user
    });
});

exports.adminBoard = asyncMiddleware(async (req, res) => {
    const user = await User.findOne({
        where: { id: req.userId },
        attributes: ["name", "username", "email"],
        include: [
            {
                model: Role,
                attributes: ["id", "name"],
                through: {
                    attributes: ["userId", "roleId"]
                }
            }
        ]
    });
    res.status(200).json({
        description: "Admin Board",
        user: user
    });
});

exports.managementBoard = asyncMiddleware(async (req, res) => {
    const user = await User.findOne({
        where: { id: req.userId },
        attributes: ["name", "username", "email"],
        include: [
            {
                model: Role,
                attributes: ["id", "name"],
                through: {
                    attributes: ["userId", "roleId"]
                }
            }
        ]
    });
    res.status(200).json({
        description: "Management Board",
        user: user
    });
});

exports.signup = asyncMiddleware(async (req, res) => {
  // Save User to Database
  console.log("Processing func -> SignUp");

  const user = await User.create({
    name: req.body.name,
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8)
  });

  const roles = await Role.findAll({
    where: {
      name: {
        [Op.or]: req.body.roles
      }
    }
  });

  await user.setRoles(roles);
  await user.setRoles(roles, {through : {status : "unblock"}});

  res.status(201).send({
    status: "User registered successfully!"
  });
});


